Setup
=====

`Download lastest blender <https://builder.blender.org/download/>`_ 

.. important::
    | Download and setup lastest available version of Blender


Upgrade
-------

There is 2 ways 

* Use the update manager in addon preferences archiapck, or 3d view Create panel "About tab"
* Request a `download link by e-mail <https://blender-archipack.org/archipack/update>`_ 

.. important::
    | In blender user preferences -> Add-ons, find Archipack PRO, and expand the panel.   
    | Disable the addon and then press "Remove" so every single old file on it will be deleted.   
    | This will preserve all your presets (they are stored in another location).   
    | Save preferences, then restart blender.   
    | Setup another version as usual.   


Download
--------

.. important::
    | Delivery process is done through blender-archipack.org
    | When you place an order, an e-mail is sent to your gumroad's e-mail adress
    | with a download link, check your spam folder and report to support if 
    | anything went wrong.


`Order and download <https://gumroad.com/l/ZRMyP>`_ 


.. important::
    | On mac os, ensure that your browser did not unzip the dowlnoaded files on the fly. 
    | Should your browser unzip, either try with another one or recompress the folder by hand.



Install the add-on on blender 4.3 +
-----------------------------------

Start blender, drag & drop downloaded archipack_xx.x.x.zip over blender's ui.



Install the add-on on blender up to 4.2
---------------------------------------

.. important::
    | Archipack lite must be disabled before installing PRO version.   
    | Disable the addon, save preferences and then restart blender.   
    

Open user preferences   
  

.. figure:: ./img/setup_user_prefs.png
   :scale: 100 %
   :alt: Open preferences
   
   Open preferences


Press "Install from file", select downloaded archipack_20.zip  


.. figure:: ./img/setup_button.png
   :scale: 100 %
   :alt: Setup
   
   Setup
   
Activate the add-on  
-------------------

.. figure:: ./img/setup_activate.png
   :scale: 100 %
   :alt: Activate
   
   Activate


Tab Category
------------

By default, Archipack's panels are in the right panel of 3d view, under Archipack tab.	
Change tab name found in "Tab Category" in add-on preferences to move them under other tab.

Use sub-menu
""""""""""""

.. figure:: ./img/add_menu.png
   :scale: 100 %
   :alt: Add Archipack menu
   
   Add Archipack menu
   
* When checked, add menu (shift+a) or view3d->add add "archipack" sub category under mesh. 
* When disabled, archipack primitives are right on mesh menu.


.. _default material library:

Default material library
------------------------

Along with the addon download link (e-mail), you'll find a link for material library file. Download, create a folder and drop the file into that folder.
In user preferences -> Add-ons -> Archpack PRO, add the full path to this folder into "Material library, Folder path"


