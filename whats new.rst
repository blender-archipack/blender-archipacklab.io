What's new ?
============

Release 2.8.1
-------------

* Add operator surface to steps (for terrains - or any surface)
* Bugfix presets not properly saved
* Bugix Pip modules setup of scipy
* Bugfix special chars proper escaping for svg export
* Bugfix roof slope with triangular ends enabled
* Remove steps distance for draw tool in preferences and make it automatic
* Add support for imperial fractions display
* Bugfix curve selection display in 2d to 3d tool
* Bugfix custom wall polling issue
* Bugfix operator collection boolean


Release 2.8.0
-------------

* Bugfix dateutil import syntax error exception
* Bugfix crash on delete with Machin3 tools and other addons
* Bugfix typo in hole delete
* Bugfix curtain generation
* Bugfix shader compilation under 3.x / 4.x
* blender 4.x compatibility
* Bugfix loosing angle when segment size is 0
* Bugfix support for inch in keyboard


Release 2.7.0
-------------

* Bugfix draw tool incorrect direction with removed segments
* Bugfix window and doors foils not updated on instances


Release 2.6.0
-------------

* Bugfix floors and molding from wall not properly matching doors
* Bugfix square manipulators not selectable
* Bugfix 2d to 3d extruded walls failing to create
* Bugfix linked windows not properly updating childs
* Remove bgl code for blender 3.7+ compatibility
* Bugfix draw tool new segment direction wrong after delete


Release 2.5.1
-------------

* Add support for shade smooth in blender 3.3
* Bugfix polygon detection openings not creating holes
* Bugfix molding/floor detection for hole object
* Bugfix kill parameters
* Bugfix kitchen object crash blender when adding cabinets by hand 


Release 2.5.0
-------------

* Bugfix draw tools not allowing navigation using shift + middle mouse
* Bugfix presets not loading material index list properly
* Bugfix copy modifier stack fails with read only attributes
* Bugfix assembly draw tool fail to create objects
* Bugfix furniture draw tool fail to create objects
* Bugfix door typo in custom hole
* Bugfix add/remove segment arrows manipulator failing
* Bugfix 2d symbol may fail when model is not properly welded
* Bugfix py deps installer fails on some unicode systems
* Bugfix kill archipack parameters fails
* Bugfix door hole size not fitting frame
* Bugfix section creation crash blender

Release 2.4.0
-------------

* Allow shift to restart wall draw tool as alt alternative for emulate 3d mouse
* Fix refpoint merge issue when there is no childs on target
* Fix svg export fail with hidden kitchen object
* Fix svg export fail to check stairs visibility
* Fix custom preset fails to load
* Fix kitchen custom door style
* Add Veil object (sunshade veil)
* Add symbol 2d operator
* Fix division by 0 in polylines
* Fix finishing removal does not update openings
* Area expose wall_area, area, perimeter and volume as parameters
* Furniture creation use selected objects if any as member
* Polylines io able to input mesh edges
* Reference point implements "take ownership" operator
* Fix slab icon
* Window Fix editing handle fails when window is bent
* Fix preset import, allow to skip or override presets
* Fix window rail panels size and alignment
* Fix window rail alignment with wall
* Expose animation in preferences
* Fix stairs creation
* Fix curveman profile enumerator
* Fix draw tool for fence and molding
* Fix beam svg export
* Update version number on migration
* Fix scipy installer for 2.93+ / 3.0
* Fix export svg ignoring curves with altitudes != 0
* Fix manipulators for curve based objects not working
* Support for 3.0
* Fix angle manipulators not working while drawing
* Expose hole margin
* Implement orthogonal draw tool for wall, molding and fence
* expose door handle altitude
* Fix icons missing in segments2
* Custom objects everywhere
* Fix roof's hip
* Implement Collada import from warehouse
* Fix ceiling real-time preset
* Fix roof's child cutter
* Draw tool to floor with snap to wall's
* Center to world operator
* Enhancement improve door frames to allow metallic like profiles
* Enhancement expose door overflow in frame
* Enhancement improve window panel location with respect to frame
* Enhancement expose Create reference point operator
* Enhancement spaces may be closed by standalone windows / doors / beams
* Enhancement window mounting  interior / exterior / nail on frame
* Enhancement window open to outside
* Enhancement window support for custom frame, including hierarchy
* Add Assembly object to group openings
* Add Furniture object to group furnitures like user defined objects
* Enhancement support user defined object in preset including hierarchy
* Enhancement support user defined Custom in windows and doors
* Enhancement support user defined object in windows and doors
* Enhancement support Custom entity load/save presets
* Fix Custom object support any level of hierarchy
* Fix Custom object flip state not working with instances
* Enhancement Custom object provide deformable option for children
* Enhancement Custom setup Operator reminds settings for objects
* Enhancement door handles support custom objects with hierarchy
* Enhancement support handles location inside / outside / both / none
* Enhancement allow different user defined handle for inside / outside
* Enhancement window handle allow user defined objects including hierarchy
* Enhancement door allow dumb user defined object and Custom based
* Enhancement window allow dumb user defined object and Custom based
* Enhancement window provide user option to set handle type
* Enhancement window alignment with respect to wall inside/outside/axis
* Enhancement user defined window and door support for automatic hole
* Enhancement standalone windows closing space between walls with snap
* Enhancement kitchen support Custom based user defined doors
* Enhancement kitchen support user defined handles
* Enhancement draw tool support for draw to floor
* Enhancement allow to save preset "category"
* Refactor objects flags are now grouped
* Fix polygon detection fails when any geometry is a point / line
* Enhancement render thumbs support many objects
* Ehnancement wall2 support for Assembly of openings
* Fix "soft" window not working when auto_synch is disabled
* Fix wall openings manipulators not setup when auto_synch is disabled
* Fix wall openings relocation fails when auto_synch is disabled
* Fix wall support for Custom object flip state
* Fix dimension not created
* Fix in area
* Fix custom draw handlers and events
* Fix 2d to 3d window handle altitude
* Add edges slope overlay operator (f3 + slope)

Release 2.3.6 
-------------

* Bugfix Area creation fails
* Bugfix Dimension creation fails
* Bugfix 2d to 3d error while creating windows


Release 2.3.5 
-------------

* Svg export now use absolute size in mm
* Turkish translation by Erder Keshkin
* Add window surface deform operator
* Auto synchro disabled by default
* Fix loosing luxcore material on object delete
* Fix Merge walls / moldings / fence performance issue
* Support for custom handle geometry
* Support for custom door / window geometry
* Support for custom woor / window hole geometry
* Hole object may cut or allow moldings
* Support for new blendebim ifc export
* Simplify objects representation on the fly for ifc body
* Fix window 2d hole representation
* Fix "freemove" creation mode for draw tools, (enable in preferences)
* Support for custom geometry for tiles / hips for roofs
* Fix roof hip not always generated
* Fix roof draft generating wrong edges in co-planar polygon
* Fix crash bug in archipack section
* Support for geometry nodes based section (experimental, breaking uvs)
* Fix gap between stairs limit too big
* Fix preset not generated when addon is outside standard path
* Add "Generic" walls presets
* Add concrete stair presets
* Fix window round / elliptic preset handles
* Fix tools tab closed by default
* Fix objects array operator negative scaling
* Allow multiple vertical rows for array operator
* Fix Quick edit only woring once
* Allow up to 128 foils in windows
* Fix performance issue with boolean exact solver

Release 2.3.4 
-------------

* Bugfix: Boolean solver now set to fast
* Bugfix: Svg exporter fails when a stair is hidden
* Bugfix: Duplicate level fails with hole entity
* Enhancement: hopscotch pattern allow different ratio between big/small tiles

Release 2.3.3 
-------------

* Blender 2.91 compatibility release
* Bugfix: Regression in fence from curve
* Enhancement: triangle from curved top shape of windows and holes (2 segments)
* Bugfix: context issue in modifier apply
* Bugfix: Slice moldings
* Bugfix: Auto synch fails with missing generator
* Feature: global auto synch switch in preferences
* Enhancement: support for 2.91 for bevel changes
* Enhancement: support for 2.90 change in modifier apply
* Bugfix: Regression in dimension manipulators
* Bugfix: Roof draft fails to properly generate faces
* Bugfix: export svg font color now respect material
* Bugfix: Custom roof shape fail with low slope
* Bugfix: Support for 2.91 API in Pip installer


Release 2.3.2
-------------

* Feature: Remove manipulator highlight removed point
* Feature: wall from curve support for grease pencil
* Feature: support for blenderbim dimensions
* Bugfix: dimension fails when measureIt Arch is not installed
* Bugfix: regression in make unique for windows / doors / hole
* Bugfix: linked openings location when manipulating size through wall
* Bugfix: door manipulator side swapped
* Bugfix: 2d to 3d precision issue in polyskel
* Bugfix: exporter shortcuts
* Bugfix: support for materials / finishings / slices in merge / add / slice
* Bugfix: support for 2.83+: changes in bevel
* Bugfix: support for 2.90+: scene ray cast
* Bugfix: support for 2.83+: bgl color management
* Bugfix: window/door draw tools restart to create linked objects 
* Bugfix: line simplify fails when there is less than 2 points
* Bugfix: blenderbim support for versions 200722+
* Bugfix: svg export fails with curve without points
* Bugfix: use defined roof without slope


Release 2.3.1
-------------

* Feature: ifc export initial support for "2d Plan" representation
* Feature: 2d Area : support for label
* Bugfix: svg export multiple holes representations
* Bugfix: svg export not hiding layouts
* Bugfix: Crash on undo with "preset" operators
* Bugfix: Context issue in disable auto manipulate
* Bugfix: Array operator not taking altitude into account
* Bugfix: Crash with luxcore materials
* BugFix: Missing icon in 2.83
* Bugfix: Wall slice/merge operator not handling openings properly
* Bugfix: ifc export fails with unassigned polygon material index
* Bugfix: svg export fails on topology errors
* Bugfix: stop auto manipulate error
* Bugfix: python deps installer fails on 2.83+
* Bugfix: terrain modifiers update issue
* Bugfix: terrain road index error
* Bugfix: wall finishing error when deleted
* Bugfix: window / door error on svg export
* Enhancement: svg export is now up to 10 x faster
* Enhancement: svg export support for css styled elements
* Enhancement: manipulator use scene unit setup for keyboard values
* Feature: section filter objects by collection
* Feature: Support for measureIt Arch 0.0.4+
* Bugfix: Simplify line remove too many vertices under some conditions
* Bugfix: 2d to 3d fails on degenerated X shaped intersections
* Bugfix: Wall snap fails on small wall segments
* Bugfix: Dimension preset store target
* Feature: Hide holes support for Luxcore and ProRender
* Bugfix: Crash with luxcore materials
* Bugfix: 2d to 3d selection display
* Bugfix: Wall snap issue on thick walls


Release 2.3.0
-------------

* Feature Export ifc
* Feature Ifc Structure create and cleanup
* German translation by Martin Bornschein
* Bugfix 2d to 3d fails on E and P shaped walls
* Bugfix blind material not loaded
* Bugfix division by zero in path manipulators
* Bugfix object from curve not properly updated
* Bugfix error when cycles is not enabled
* Bugfix roof cutter / terrain cutter not updating parent on delete
* Bugfix manipulator location may fail
* Bugfix geometry filter
* Bugfix throttle error when object name change in the between
* Bugfix merge close sources Polygon skeleton
* Bugfix presets with finishings
* Feature path manipulator use G shortcut to move points, and better select mode
* Bugfix Roof better user defined roof detection
* Bugfix Wall and slab uv not always properly unwrapped
* Enhancement context less low level operations
* Bugfix thumbs generator support for curves
* Bugfix thumbs generator transparent background changes in 2.82


Release 2.2.7
-------------

* Feature Wall allow finishing on start and end faces
* Feature Allow to disable automatic boolean on Door and Window
* Feature Window sill altitudes
* Feature add default uv to handles
* Feature Door spherical handle
* Feature Door array operator
* Feature Door create standalone door operator
* Feature Door without frame
* Feature Door allow negative bevel for panels
* Feature array ui now display rule nesting
* Feature add option to display path manipulator as small line
* Feature add shortcut option for path manipulator in preferences
* Feature wall are now able to snap on wall start and end
* Bugfix in wall snap : way more predictable and reliable
* Enhaced roof "Roman" covering shape
* Feature merge wall2 to single geometry operator (for export to game engine)
* Bugfix division by zero on wall
* Bugfix array rotate on corner affect scale
* Bugfix array remove part change generator order
* Bugfix array ui not properly updated when remove all geometry
* Bugfix slab fails to add 2nd balcony
* Regression : temporary disable animation support


Release 2.2.6
-------------

* Bugfix wall segment parameters not showing
* Bugfix error on window / hole array when there is not enough space


Release 2.2.5
-------------

* Bugfix wall merge auto boolean and openings orientation
* Bugfix wall snap precision issue
* Bugfix error when deleting wall / fence / molding in draw mode
* Bugfix Manipulator SPACE conflict with full screen toggle
* Bugfix Manipulators limit add and split to A and S keys without modifiers
* Enhancement path merge at segment intersection
* Enhancement reference point duplicate now allow x, y, and z offset
* Fix Ceiling "auto-sychro" error
* Enhancement Terrain allow "Satellite" map uv projection
* Feature experimental Array entity
* Fix Roof round tiles shape
* Fix Roof missing tiles near top of roof
* Fix Roof tiles overlay
* Bugfix Roof cutter rotation
* Bugfix Roof cutter segments ui
* Bugfix nested Roof childs altitude 
* Add random seed in Roof and Floor
* Feature Fence allow negative space for panels
* Feature more precision in stair height / footstep display
* Add move up/down operators for kitchen cabinets
* Add merge kitchen operator
* Add support for window out frame material
* Add support for window glass location / thickness
* Enhancement Window / Hole array support for curved segments
* Enhancement Window / Hole bend no more "dead area" when manipulating
* Feature limited support for archipack parameter animation
* Bugfix svg export fails with crossing walls
* Bugfix svg export fails on single wall with openings
* Add svg export support for multi line text
* Bugfix section fails to remove temp objects when not crossing anything
* Fix potential issues with physical atmosphere addon
* Bugfix material preview
* Add a tool to remove vertex colors for visible objects
* Add a global vertex colors parameter in prefs to disable generation
* Add support for altitude in segments
* Add support for explicit collection name when duplicate object
* Add support for labels with icons
* Add support for custom script path
* Bugfix move preset window location


Release 2.2.4
-------------

* Feature Ceiling object
* Feature Support for wall's wood cladding finishing
* Feature Create standalone window operator
* Feature Split and Merge walls, fences and moldings
* Feature Add and remove segment manipulators for walls / fence / moldings / slab / floors / ceiling / roof draft / cutter
* Fix wall geometry now remains stable when changing base line from inside / axis / outside and flip inside / outside
* Feature offset and apply offset operator for wall 
* Fix switching workspace error when manipulating
* Fix hole not properly detect floor area
* Fix molding from wall now skip single vertices curves
* Fix check of geometry in wall to curve
* Fix closed wall representation in export .svg
* Spanish translation revision from Sara Gonzalez
* Fix roof draft segment edition not working through ui
* Improved uv unwrap for patterns on walls / floors / ceilings
* Fix Slab side material index not working when angle is under 1°
* Fix roof presets skylight loosing parent segment
* Fix unhide / enable collections when needed
* Cleanup blind bend
* Fix error in native experimental ifc exporter on some systems

Release 2.2.3
-------------

* Fix from curve object's origin depending on curve orientation
* Fix hole not hidden by windows/hole arrays
* Feature support for 2d to 3d walls in duplicate level operators
* Fix allow roof segments up to 10k units
* Fix holes not properly detected by floors / moldings
* Ifc export fix for windows
* Windows separated glass
* Allow to restart "draw wall" tool on any wall by keeping ALT pressed
* Manipulators handles facing view
* Feature operator for ifc naming convention cleanup
* Feature add operator to apply random uvs for game engine export
* Fix preset loader not working after draw a wall
* Fix typo in moldings collection name
* Fix door minimum wall thicnkess (lower to 3 cm instead of 10cm)
* Fix regression in auto synchro with square manipulators
* Fix 2d to 3d selection no more working after undo
* Fix zero division error in roof
* Fix snap issue in walls with small width
* Fix material index of inside 10x240 wall preset
* Fix superscript for 2 and 3 missing with blender units
* Fix wall manipulators setup not properly handling CW mode

Release 2.2.2
-------------

* Russian translation by Demid Wolf
* Parametric Hole object
* Add Triangulate operator for game engine export
* fix floors/slab/molding from wall failing with round wall segments
* fix handles subsurf level to 2
* fix wall uvs continuity for "in" side 

Release 2.2.1
-------------

* Italian translation by Luca Pierantozzi
* Add support for Filmic addon
* Bugfix py deps installer fails on windows 7
* refactory addon updater client / server side

Release 2.2.0
-------------

* fix reference point merge
* fix regression in 2d to 3d wall generator
* fix doors and windows default material on create
* fix layers in 2d to 3d
* fix for view layer
* floor default to (not so) slow boolean - but way safer

Release 2.1.0
-------------

* fix beam normals
* fix floor “Realtime” randomly flipped normals
* fix door soil normals
* fix altitude for custom objects
* enable terrain by default - fix for missing icons
* addon updater to make it easy to check for updates and request download link when available (About panel of create tab)

Release 2.0.10
-------------

* Fix issues with section objects.

Release 2.0.9
-------------

* hot fix for auto-manipulate issues

Release 2.0.8
-------------

* Fix crash with auto-manipulate enabled when drawing walls
* Fix thumbnails generator for presets
* Fix brep in .ifc export

Release 2.0.7
-------------

* Fast terrain generation with optional support for scipy (up to 20x faster)
* Fast floor / roof covering boundary / cutters
* Better geometry and easy manipulation of "soft windows" on wall's edges
* bugfix in angle manipulators
* bugfix in 2d export
* bugfix in multi dimensions
* bugfix curved windows rod
* Spanish translation by Sara González
* Rough experimental ifc exporter - still in alpha stage
* bugfix in translation system for operators
* Experimental python modules installer

Release 2.0.6
-------------

* Soft windows following wall shape
* Roof draft object
* Support for roofs from user defined polygons
* Material management tab
* Enhanced Custom object setup
* Cutters: support for "Union" operation, perfect for roofs overhangs
* 14 Languages
* Setting for presets thumbs size in preferences 
* Enhanced "Constant handle size" 
* Enhanced roof bevel and fix for tiles across valley
* Roof randomness for tiles
* Support for rotation of reference points in "3d space"
* Enhanced draw tools for fence and moldings (build on xy plane when not snapping)

Release 2.0.5
-------------

* Enhanced terrain
* Draw tools for moldings and fences
* Array tool for windows and custom openings
* Wall finishing
* Duplicate tools in reference point, including collection instance mode
* Chinese and french translations

Release 2.0.4
-------------

* Terrain (experimental feature)
* Refactory 2d to 3d polygon to wall
* Reference point "to building collection"
* Fastest floor generation (up to 5x)
* Floor and molding support for inside spaces not touching outside walls
* Support for holes in Area 2d objects
* Support for offset of walls patterns
* Bugfix wall patterns not aligned across faces
* Fix for regressions of tranlation system
* BugFix stairs user defined parts
* Bugfix roof from wall crash
* Bugfix fence and rail fence profile
* Bugfix material not properly saved into library

Release 2.0.3
-------------

* Compatibility fix
* Support for internationalization
* Support for offset of floors patterns

Release 2.0.2
-------------

* Architectural Sunlight
* Area / volume 2d object
* On screen Translation system
* Create collection and fast duplicate of level reference point tools


Releas 2.0.1
------------

* Hung Windows
* Curtain generator


Release 2.0.0
-------------

* Blender 2.8 compatibility
* Auto manipulate
* Cleaner ui using tabs
* Save materials to library
* Wall snap sides
* Fast wall synchro of objects
* Walls tools available into wall object parameters
* Material indexes setup by material name
* Use of collection to structure projects
* Generic delete operator taking care of objects relationship
* Auto-manipulate
* Auto backup while manipulating
* Realtime manipulate using object's degenerate temporary state
* User feed back using cursor "wait"
* Random vertex colors for roofs, fences, stairs and floors
* Simplifyed material library
* Kitchen materials override by cabinet and doors
* Wall materials override by segment
* Door step
* Improved window panel (joints and center parts)
* Slab and slab cutter materials as per segment
* Reference points duplicate/delete level objects tools
* Remove / merge reference points tools
* Improved delete holes
* Better custom windows/doors setup
* Fastest preset saving using eevee
* Better wall fit roof 


Release 1.4.0
-------------

* Better manipulators 
* Better intersection code for arcs
* Basic abstraction layer for 2.8
* Layer management
* Support for user defined units in dimensions
* Better handling of objects creation right in 3d
* Fence: user defined material for posts
* Manipulators: support for more than 2 properties
* Material: fastest assignment
* Molding: fastest preset loading
* Drop support for manipulators gl rendering
* Better reference point management
* Support for curved segment bi-directional manipulators
* Refactor type change for segments
* Cleanup ui, more consistent across objects
* Wall auto synch speed improvement
* Wall location with respect to base line easyer to setup (axis, left, right) 
* Wall support for offset


Release 1.3.8
-------------

* Improved stability

* Slab from wall auto-adjust offset to fit wall outside
* Floor from wall advanced auto-detection of childs walls / slab cuts / doors / windows
* Moldings with advanced tool moldings from wall
* Floor support for rotation of patterns
* Wall to curve (inside, outside, both, 2d symbol with openings, floors boundarys)
* Stair to curve (2d symbol, hole as curve for slab/floors cutters)
* Slab, Floor, Roof and Wall: add proper delete tool
* Slab, Floor, Fence, Wall and Cutters from curve: add support for scaled curve
* Autoboolean now use intersection based wall holes ownership detection (wall2 only)
* Reference point shape flat arrows to ease snapping 
* Bugfix: kitchen cabinets dosent allow hole on 1st cab
* Bugfix: pygeos relate operation not working as expected (contains / touches / intersects.. relationship)
* Bugfix: wall childs detection on open walls leading to wrong relocation
* Bugfix: dynamic material enumerator not properly holding reference
* Bugfix: wall relocate childs fails on Split and Remove segment 
* Bugfix: 2d to 3d create wall not always working as expected (view manager clipping error)
* Single operator to manipulate: bpy.ops.archipack.manipulate()
* Remove support for depricated boolean carve mode
* Improve presets parameters filtering on save
* Loading preset modify all selected objects
* Better icons and context check for tools (create objects from wall)
* Reorder create panel so it match typical workflow
* Fence from curve use curve resolution at create time


* 2d

  * Section and Camera section objects
  * Multi-dimensions object bound to "measurable points" selectable right on screen
  * Layout object to precisely define "paper space" for export
  * symbol for windows
  * symbol for doors
  * symbol for walls including support for childs walls
  * symbol for stairs
  * Basic dimension object
  * 2d svg exporter
  
* floor heating pipes designer (alpha)

  
Release 1.3.7
-------------

* Kitchen cabinets see :ref:`kitchen` 
* Window Shutters see :ref:`window components`
  
  * Add parameter for panels depth and borders
  * Add parameter for hinge size
  * Bugfix: wrong location of hinge when shape is not rectangle
  
* Add parameters for window panels profile size
* Improvement of frame shape for rail windows
* Add global offset parameter for slabs
* Bugfix: wall T parent not always synch child
* Bugfix: blind not properly created by windows
* Bugfix: wrong slab geometry with cw wall


Release 1.3.6
-------------

* Window shutters see :ref:`window components`
* Blind object see :ref:`blind`
* Update documentation link


Release 1.3.5
--------------

* 2d to 3d

  * Use pygeos instead of shapely
  * Improvement of Detect tools workflow (1 single button and then keyboard)
  * Add 2d boolean
  * Add buffer
  * Add offset
  * Add simplify
   
* 2d to 3d tutorial video
* documentation
