Welcome to Archipack documentation!
===================================

.. figure:: ./img/archipack.png
   :scale: 100 %
   :alt: Archipack
   
Archipack for Blender is a toolset for architectural visualisation

`Find Archipack on youtube <https://www.youtube.com/stephen_leger/videos>`_
----------------------------------------------------------------------------------------------


.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   setup
   whats new
   user guide
   extending archipack
   about

