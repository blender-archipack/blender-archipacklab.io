Internationalization
====================

Archipack 2.0.2 does implement on-screen interface to translate the addon.  

`Translation progress <https://trello.com/c/MB442znV/11-internationalization>`_


.. figure:: ../img/internationalization.png
   :scale: 100 %
   :alt: On screen forms for tranlsations

   Translate: on screen forms to ease translations


Usage
-----

 * Enable translation in user preferences -> interface, and choose your locale.
 * In achipack prefs, check "Enable translation"
 * You'll then find "Refresh translation" and "Save translation" at the top of every panel.
 * When you expand any part of the ui, press "refresh" so the addon is able to display translations fields (labels and tooltips)
 * When done or at any time youre able to "Save translation"
 * There is also a small eye at the right of "Save translation" to hide translate fields.


Submit a language
-----------------

In addon preferences, when translate is enabled, 2 new buttons are avaliable

 * Find translation file open a file navigator at tranlation file location
 * Send tranlation open up a web browser and then your e-mail client so you're able to attach lang.json file and submit  @blender archipack 


