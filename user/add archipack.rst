Add Archipack
=============

Add archipack panel.

.. figure:: ../img/ui_add_panel.png
   :scale: 100 %
   :alt: Add Archipack panel
   
   Add Archipack panel

.. tip::
	By default, "Add archipack" panel is in the left panel of 3d view, under Archipack tab.
	
	Change "Create" tab name found in "Tab Category" in add-on preferences to move it under other tab.
   
   
Add archipack menu (shift+a) or 3d view -> Add menu

.. figure:: ../img/add_menu.png
   :scale: 100 %
   :alt: Add Archipack menu
   
   Add Archipack menu
   
.. tip::
	By default archipack primitives are in "archipack" sub menu.
	
	You may uncheck "Use Sub-menu" in add-on preferences to get buttons right in the main mesh menu.   
   
   
Window
------

.. figure:: ../img/add_window.png
   :scale: 100 %
   :alt: Add Window
   	
   Add Window
   
Add
"""

Add a window at cursor location

Draw
""""

Draw tool display a preset chooser, and allow to "Paint" windows over walls.
By default, windows are linked (modifications on any does modify all)

 * Press shift to make unique window (non linked)
 * Press alt on invoke, to use current selected windows as preset, and draw linked windows.
 * Press c while drawing to display the preset chooser again so you wont have to restart the tool to choose another window.

.. tip::
	Draw tool automatically create interactive holes on the wall.
	Holes update in real-time when you move the window or edit any parameter.

 
Door
----

.. figure:: ../img/add_door.png
   :scale: 100 %
   :alt: Add Door
   	
   Add Door
   
Add
"""

Add a door at cursor location

Draw
""""

Draw tool display a preset chooser, and allow to "Paint" door over walls.
By default, doors are linked (modifications on any does modify all)

 * Press shift to make unique door (non linked)
 * Press alt on invoke, to use current selected doors as preset, and draw linked doors.
 * Press c while drawing to display the preset chooser again so you wont have to restart the tool to choose another door.

.. tip::
	Draw tool automatically create interactive holes on the wall.
	Holes update in real-time when you move the door or edit any parameter.

	
Stair
-----

.. figure:: ../img/add_stair.png
   :scale: 100 %
   :alt: Add Stair
   	
   Add Stair
   
Add
"""

Add a stair at cursor location

Wall
----

.. figure:: ../img/add_wall.png
   :scale: 100 %
   :alt: Add Wall
   	
   Add Wall
  
Add
"""

Add a wall at cursor location

Draw
""""

Draw tool allow to "Paint" wall.

 * Press ctrl to enable snap
 * Press x or y enable constraint, subsequent press on x y toggle between local and global axis
 * Use keyboard and type in length of the segment

In order to close a wall with the draw tool, simply snap the last segment to first one. 

Wall from curve(s)
""""""""""""""""""

Build a wall for each selected curve


Fence
-----

.. figure:: ../img/add_fence.png
   :scale: 100 %
   :alt: Add Fence
   	
   Add Fence
  
Add
"""

Add a fence at cursor location

Fence from curve
""""""""""""""""

Build a fence for each selected curve


Truss
-----

.. figure:: ../img/add_truss.png
   :scale: 100 %
   :alt: Add Truss
   	
   Add Truss
  
Add
"""

Add a truss at cursor location


Slab
----

.. figure:: ../img/add_slab.png
   :scale: 100 %
   :alt: Add Slab
   	
   Add Slab
  
Slab from curve
""""""""""""""""

Build a slab from selected curve

Wall from slab
""""""""""""""

Create a wall over selected slab

Slab from Wall
""""""""""""""

Create a slab under selected wall

Ceiling from Wall
"""""""""""""""""

Create a slab over selected wall


Roof
----

.. figure:: ../img/add_roof.png
   :scale: 100 %
   :alt: Add Roof
   	
   Add Roof
  
Add
"""

Create a roof at cursor location




Floor
-----

.. figure:: ../img/add_floor.png
   :scale: 100 %
   :alt: Add Floor
   	
   Add Floor
  
Add
"""

Create a floor at cursor location

Floor from wall
"""""""""""""""

Create a floor inside selected wall

Floor from curve
""""""""""""""""

Create a floor using selected curve as boundary


Blind
-----

.. figure:: ../img/add_blind.png
   :scale: 100 %
   :alt: Add Blind
   	
   Add Blind
  
Add
"""

Create a blind at cursor location

 
Kitchen
-------

.. figure:: ../img/add_kitchen.png
   :scale: 100 %
   :alt: Add Kitchen
   	
   Add Kitchen
   
Add
"""

Add a kitchen at cursor location


Custom objects
--------------

Use custom mesh as wall or hole
see :ref:`custom objects`

.. figure:: ../img/add_custom.png
   :scale: 100 %
   :alt: Add Custom
   	
   Add Custom
   



