Archipack objects
=================

Archipack objects are parametric, this means you may alter parameters at any time and objects update in real-time.
Find objects parameters in the right panel of 3d view (shortcut n by default).

.. tip::
	Archipack does support linked copy (alt+d) so when altering one, linked objects update according.

	Parameters also support "Copy to selected". Select target objects, then source one, right click on parameter to share and use "Copy to selected".
	
Common properties
-----------------


Archipack material
""""""""""""""""""

see :ref:`archipack materials`

.. figure:: ../img/ui_archipack_material.png
   :scale: 100 %
   :alt: Archipack material selector

   Select / create / delete material sets for objects
 

Manipulate
""""""""""

see :ref:`manipulate mode`

.. figure:: ../img/ui_manipulate.png
   :scale: 100 %
   :alt: Manipulate button

   Manipulate: auto display manipulators on screen when selecting objects

   
Presets
"""""""

Objects with preset feature provide buttons to allow saving / removing and reloading user defined presets.
The preset system does render a thumbnail on save - thumbnails are 150 x 100 pixels.

.. figure:: ../img/ui_preset.png
   :scale: 100 %
   :alt: Preset selector
   
   Select / create / delete presets for objects

.. tip::
	Archipack provide a convenient way to share presets across computers. In file menu -> import / export find "Archipack preset (.apk)". Exporting pack all presets into a .zip like file, Import simply unzip those presets into presets folder, keeping existing ones.  
   
   
Window
------

Available utility
"""""""""""""""""

 * Delete: take care of removing every component of the window
 * Refresh: update linked windows with missing parts (eg on copy)
 * Unlink: make this window unique, only available on linked windows

.. figure:: ../img/ui_win_door_utils.png
   :scale: 100 %
   :alt: Utility buttons
   
   Utility buttons
 
	
Main properties
"""""""""""""""

 * Type: window type in [Swing, Rail]
 * Width: of opening 
 * Depth: of wall
 * Height: of opening
 * Altitude: bottom of opening, from pivot point
 * Offset: offset of frame from inside part
 * Shape: window shape in [Rectangle, Top elliptic, Top round, Top oblique, Full circle]

.. figure:: ../img/ui_window_main.png
   :scale: 100 %
   :alt: Main window properties

   Main window properties
 
.. _window components:

Components 
""""""""""

.. figure:: ../img/ui_window_components.png
   :scale: 100 %
   :alt: Window components

   Window components
 

Frame
##### 
.. versionadded:: 1.3.5

   * Overflow : how much the frame overflow opening

Handle
######
 * Altitude from pivot point
 
Out frame
#########

 * Front width: width of the front part of the frame
 * Front depth: depth of the front part of the frame
 * Side depth: depth of side part of frame
 * Offset: Overflow of hole to make negative joints aroud frame

Blind
#####

.. versionadded:: 1.3.6

see :ref:`blind`

 * Blind inside: Add parametric blind inside 
 * Blind outside: Add parametric blind outside 

Shutter
#######

.. versionadded:: 1.3.6

 * #left: shutters on left side 
 * #right: shutters on left side
 
Rows
""""

.. figure:: ../img/ui_window_rows.png
   :scale: 100 %
   :alt: Window rows

   Rows and columns of panels

Parameters
##########

 * Number of rows
 * Panels: number of columns for current row
 * col %: percent of width for the panel
 * fixed: type of frame

Portal
######

.. figure:: ../img/ui_window_portal.png
   :scale: 100 %
   :alt: Window portal light

   Generate a portal light for this window
 
 
.. tip::

	Open a window ?
	
	Panels are separate objects, so you are able to select and rotate/slide the way you want.
	
	Bend window to fit a curved wall ?
	
	Add a subdivide modifier in "simple" mode with say 2 or more subdivisions.
	Add a SimpleDeform modifier in "Bend" mode and set angle according your needs.
	Do the same with the hole object, and each panels.

Materials
"""""""""

.. figure:: ../img/ui_window_materials.png
   :scale: 100 %
   :alt: Window materials

   Set index of materials for windows components
	
	
Door
----

Available utility
"""""""""""""""""
   
 * Delete: take care of removing every component of the door
 * Refresh: update linked doors with missing parts (eg on copy)
 * Unlink: make this door unique, only available on linked doors

.. figure:: ../img/ui_win_door_utils.png
   :scale: 100 %
   :alt: Utility buttons
   
   Utility buttons	
	
Size
""""
   
 * Width: of opening 
 * Depth: of wall
 * Height: of opening
 * Offset: offset of frame from inside part


.. figure:: ../img/ui_door_main.png
   :scale: 100 %
   :alt: Door size

   Door size 
 
Door
""""

.. figure:: ../img/ui_door_door.png
   :scale: 100 %
   :alt: Door

   Door
   
Frame
"""""

.. figure:: ../img/ui_door_frame.png
   :scale: 100 %
   :alt: Door frame

   Door frame

Panels
""""""

 * Model: shape of the front part of the door panel
 * Distribution or sub parts in [Regular, 1/3]
 * h: number of horizontal subparts 
 * v: number of vertical subparts
 * Bottom: space of subparts from bottom
 * Spacing: spacing between subparts
 * Border: spacing of subparts from border
 * Bevel: bevel amount of subparts

.. figure:: ../img/ui_door_panels.png
   :scale: 100 %
   :alt: Door panels style

   Door panels style 
 
.. tip::
	Open a door ?
	
	Panels are separate objects, so you are able to select and rotate/slide the way you want.

	
Materials
"""""""""

.. figure:: ../img/ui_door_materials.png
   :scale: 100 %
   :alt: Door materials

   Set index of materials for door components
		
 
Stair
-----
 
Presets
"""""""

.. figure:: ../img/ui_stair_presets.png
   :scale: 100 %
   :alt: Stair presets

   Stair presets

Beside saved presets, stair implements basic presets including I, L, U, O and user defined overall shapes.

Main properties
"""""""""""""""

.. figure:: ../img/ui_stair_main.png
   :scale: 100 %
   :alt: Stair properties

   Stair properties
   
Parts
"""""

.. figure:: ../img/ui_stair_parts.png
   :scale: 100 %
   :alt: Stair parts

   Stair parts
  
Stairs allow unlimited number of parts  

 * Parts type in [Straight, Curve, Dual-curve] stair and landing  
 * Curved parts allow border shape in [Straight, Curve]   
   
Steps
"""""

.. figure:: ../img/ui_stair_steps.png
   :scale: 100 %
   :alt: Steps

   Steps

   
Handrail
""""""""

.. figure:: ../img/ui_stair_handrail.png
   :scale: 100 %
   :alt: Handrail

   Handrail


String
""""""

.. figure:: ../img/ui_stair_string.png
   :scale: 100 %
   :alt: String

   String


Post
""""

.. figure:: ../img/ui_stair_post.png
   :scale: 100 %
   :alt: Post

   Post
   
.. tip::
	Advanced use of defined custom mesh as post using vertex groups
	 * "Bottom" vertex group vertices follow steps
	 * "Slope" vertex group vertices are affected by overall slope.


Subs
""""

.. figure:: ../img/ui_stair_subs.png
   :scale: 100 %
   :alt: Subs

   Subs
   
.. tip::
	Advanced use of defined custom mesh as subs using vertex groups
	 * "Bottom" vertex group vertices follow either steps or slope (as defined by Bottom)  
	 * "Slope" vertex group vertices are affected by overall slope.
   
Panels
""""""

.. figure:: ../img/ui_stair_panels.png
   :scale: 100 %
   :alt: Panels

   Panels
   

Rails
"""""

.. figure:: ../img/ui_stair_rails.png
   :scale: 100 %
   :alt: Rails

   Rails


Materials
"""""""""

.. figure:: ../img/ui_stair_materials.png
   :scale: 100 %
   :alt: Materials

   Materials
  
Define material indexes for stair components
      
   
Wall
----

.. note::
	2d to 3d walls are not Archipack walls objects

.. tip::
	When drawing a wall, snapping the last segment to the first one automatically close your wall.

	
Main properties
"""""""""""""""

.. figure:: ../img/ui_wall_main.png
   :scale: 100 %
   :alt: Wall

   Wall

Walls properties

 * Width: width of the wall
 * Height: Height of the wall
 * Floor thickness: thickness of floors, define the inside finite level 0
 * Base line: in [Outside, inside, axis] wall location around manipulable axis.
 * Flip in/out: flip wall's inside/outside
 * Close: close the wall
 * Dimensions: add/remove auto dimensions for this wall
 * Fit roof / Auto-fit: fit a surronding roof
 * Auto synchro: adapt surrounding objects when manipulating wall
 * Auto snap wall: snap start and end of wall


.. note::
	Auto-synch may produce incorrect results, so use with caution
   

Wall path
"""""""""

 * Split: cut a segment in two parts
 * Remove: remove current segment
 * Splits: split segment to adjust altitude of sub parts
 * alt: altitude of sub part
 * pos: location of split along segment
 * angle: split angle
 
.. figure:: ../img/ui_wall_parts.png
   :scale: 100 %
   :alt: Wall parts

   Wall parts

.. tip::
	Wall part 1 angle define rotation about parent wall segment when wall is a T child



Finishings
""""""""""

 * Enable finishings

   * (+) add a finishing
   * location: index of the finishing
   * altitude: bottom altitude of finishing
   * height: maximum height of finishing
   * finishing type in [horizontal boards, vertical boards, logs, tiles, user defined]
   * material index of finishing
   * width of finishing
   * thikness of finishing 
 
 * Finishing index for inside and outside
 * Override finishing indexes for each wall segment

 
Materials
"""""""""

.. figure:: ../img/ui_wall_materials.png
   :scale: 100 %
   :alt: Materials

   Materials
  
Define material indexes for wall / walls segments


Tools
"""""

.. figure:: ../img/ui_wall_utils.png
   :scale: 100 %
   :alt: Wall utils

   Wall tools
   
Create objects fitting wall:

 * Create slabs / ceiling slabs
 * Create floors
 * Create moldings / ceiling moldings 
 * Create roofs
 * Auto-boolean tool to refresh missing booleans 
 * Create curves


Slab
----

Creating slabs
""""""""""""""

 #. Either select a curve (boundary of your slab), or a wall.
 #. Use appropriate tool found in "Add archipack" panel (slab from curve)
 #. Select a wall and use slab from wall from wall's parameters, tab tools
 
Main properties
"""""""""""""""

 * Slab cutter: create a cutter to make hole(s) on the slab
 
.. figure:: ../img/ui_slab_main.png
   :scale: 100 %
   :alt: Slab

   Slab

	
Path
""""

 * Balcony: Create a balcony from the segment, with a fence. Fence is kept in synch with slab on edit.  
 
.. figure:: ../img/ui_slab_parts.png
   :scale: 100 %
   :alt: Slab parts

   Slab parts

 
 
 
Slab cutter
-----------

.. figure:: ../img/ui_slab_cutter.png
   :scale: 100 %
   :alt: Slab cutter

   Slab cutter

Slab cutter does cut holes on the slab.

Path
""""
 * Mode: boolean operation in [Difference, Intersection]
 * From curve: use a curve as input to define overall shape of the cutter

Materials
"""""""""
 * Material indexes for sides
 * Override materials by segment
 
 
Fence
-----

Main properties
"""""""""""""""

.. figure:: ../img/ui_fence_main.png
   :scale: 100 %
   :alt: Fence

   Fence

Parts
"""""

.. figure:: ../img/ui_fence_parts.png
   :scale: 100 %
   :alt: Fence parts

   Fence parts   
   
Handrail
""""""""

.. figure:: ../img/ui_fence_handrail.png
   :scale: 100 %
   :alt: Handrail

   Handrail


Post
""""

.. figure:: ../img/ui_fence_post.png
   :scale: 100 %
   :alt: Post

   Post
   
.. tip::
	Advanced use of defined custom mesh as post using vertex groups
	 * "Bottom" vertex group vertices follow steps
	 * "Slope" vertex group vertices are affected by overall slope.


Subs
""""

.. figure:: ../img/ui_fence_subs.png
   :scale: 100 %
   :alt: Subs

   Subs
   
.. tip::
	Advanced use of defined custom mesh as subs using vertex groups
	 * "Bottom" vertex group vertices follow either steps or slope (as defined by Bottom)  
	 * "Slope" vertex group vertices are affected by overall slope.
   
Panels
""""""

.. figure:: ../img/ui_fence_panels.png
   :scale: 100 %
   :alt: Panels

   Panels
   

Rails
"""""

.. figure:: ../img/ui_fence_rails.png
   :scale: 100 %
   :alt: Rails

   Rails


Materials
"""""""""

.. figure:: ../img/ui_fence_materials.png
   :scale: 100 %
   :alt: Materials

   Materials
  
Define material indexes for fence components
  

Floor
-----

Main properties
"""""""""""""""

 * Floor cutter: create a cutter to make hole(s) on the floor
 * From curve: use a curve to define boundary shape
 
.. figure:: ../img/ui_floor_main.png
   :scale: 100 %
   :alt: Floor

   Floor

Parts
"""""

.. figure:: ../img/ui_floor_parts.png
   :scale: 100 %
   :alt: Floor parts

   Floor parts   
   
Components
""""""""""

 * Floor patterns 
	* Windmill
	* Hexagon
	* Stepping stone
	* Hopscotch
	* Regular tiles
	* Herringbome
	* Herringbone parquet
	* Square parquet
	* Board
 * Random thickness
 * Solidify: Extrude tiles
 * Grout: Make grout under tiles
 * Bevel: Apply a bevel on tiles borders
 * Random Material: Randomize material ids of tiles
    

.. figure:: ../img/ui_floor_components.png
   :scale: 100 %
   :alt: Floor components

   Floor components

Floor cutter
------------

.. figure:: ../img/ui_floor_cutter.png
   :scale: 100 %
   :alt: Floor cutter

   Floor cutter

Floor cutter does cut holes on the floor.

Options
"""""""
 * Mode: boolean operation in [Difference, Intersection]
 * From curve: use a curve as input to define overall shape of the cutter
   
   
Roof
----

.. figure:: ../img/roof_cutter_low.png
   :scale: 100 %
   :alt: Archipack Roof

Utility
"""""""

 * Parent: link as child of another roof
 * Roof Cutter: create a cutter to make hole / cut borders of the roof
 
.. figure:: ../img/ui_roof_main.png
   :scale: 100 %
   :alt: Roof utility
   
   Roof utility
   
   
Main properties
"""""""""""""""
 * Draft mode: Only draw external edges for fast shape prototyping 
 * Slope: slope in percent of width
 * Width: width of the first part
 
.. figure:: ../img/ui_roof_size.png
   :scale: 100 %
   :alt: Roof size
   
   Roof size (basic)
 
Parts
"""""

.. figure:: ../img/ui_roof_parts.png
   :scale: 100 %
   :alt: Roof parts
   
   Roof parts

Covering
""""""""

 * Covering
	* Eternit®
	* Braas® 1 (tile 1 ondulation)
	* Braas® 2 (tile 2 ondulations)
	* Lauze stone 
	* Roman
	* Round
	* Square
	* Ondule
	* Metal
	
.. figure:: ../img/ui_roof_covering.png
   :scale: 100 %
   :alt: Covering
   
   Covering

Hip
"""

.. figure:: ../img/ui_roof_hip.png
   :scale: 100 %
   :alt: Hip
   
   Hip
   
Beam
""""

.. figure:: ../img/ui_roof_beam.png
   :scale: 100 %
   :alt: Beam
   
   Beam

Gutter
""""""

.. figure:: ../img/ui_roof_gutter.png
   :scale: 100 %
   :alt: Gutter
   
   Gutter
   

Fascia
""""""

.. figure:: ../img/ui_roof_fascia.png
   :scale: 100 %
   :alt: Fascia
   
   Fascia   

Bargeboard
""""""""""

.. figure:: ../img/ui_roof_bargeboard.png
   :scale: 100 %
   :alt: Bargeboard
   
   Bargeboard
   
   
Roof cutter
-----------

Options
"""""""
 * Mode: boolean operation in [Difference, Intersection]
 * From curve: use a curve as input to define overall shape of the cutter


.. figure:: ../img/ui_roof_cutter.png
   :scale: 100 %
   :alt: Roof cutter

   Roof cutter

Roof cutter does cut the roof either to create hole or on borders.
 
Parts
"""""

 * Border type
	* Side: Add bargeboards
	* Side link: Raw cut between linked parts
	* Top: Add hip
	* Bottom: Add gutter


.. _blind:

Blind
-----
.. versionadded:: 1.3.6

Blinds are designed to be created from window object see :ref:`window components`, 
taking care of size, basic location and parenting blind to window. 
Once created you are able to change blind parameters according your needs.


Presets
"""""""

.. figure:: ../img/ui_blind_presets.png
   :scale: 100 %
   :alt: Blind presets

   Blind presets

Beside saved presets, blinds implements basic presets including
 * Venitian
 * Slat
 * Roller curtain
 * Blades
 * Plated
 * Japanese
 * Vertical slotted (Maui)

Main properties
"""""""""""""""

.. figure:: ../img/ui_blind_main.png
   :scale: 100 %
   :alt: Blind properties

   Blind properties
   
Slat
""""

.. figure:: ../img/ui_blind_slat.png
   :scale: 100 %
   :alt: Slat properties

   Slat properties (may vary occording style)
   

   
Truss
-----

Main properties
"""""""""""""""

 * Type
	* Prolyte® E20 
	* Prolyte® X30 
	* Prolyte® H30
	* Prolyte® H40
	* Opti Trilite® 100
	* Opti Trilite® 200
	* User defined

.. figure:: ../img/ui_truss.png
   :scale: 100 %
   :alt: Truss

   Truss

   
   
.. _kitchen:

Kitchen
-------
.. versionadded:: 1.3.7   

Kitchen is a powerfull cabinets generator aimed at but not limited to kitchens.


Main properties
"""""""""""""""

.. figure:: ../img/ui_kitchen_main.png
   :scale: 100 %
   :alt: Kitchen properties

   Kitchen properties

.. tip::
	Manipluate mode does show cabinet number on top of each cabinet
	
Height mode
 * Height multiplier: use height basis as module height size, and a multiplier to set number of height modules of elements
 * Absolute height: use absolute heights

.. note::
	Height multiplier allow fast vertical resizing, and common kitchen cabinets rely on "modular" heights
	
	Overriding default height of doors is always possible by setting multiplier to 0
	
	
Default: ground cabinets
 * Height: cabinet height (either absolute or multiplier)
 * Depth: cabinet depth
 
Wall: wall cabinets 
 * Height: cabinet height (either absolute or multiplier)
 * Depth: cabinet depth

Full: full height cabinets
 * Height: cabinet height (either absolute or multiplier)

Thickness of boards

Doors style
 * Board
 * Board with bevel
 * Board with frame
 * Board with frame and bevel

Doors size
 * Thickness: thickness of door
 * Border: frame size
 * Chanfer: bevel size

Handles
 * Style
 * Offset from border / top 
 * Offset from top / bottom, apply on doors only 
 * Width, Depth, Height / Diameter: size of handle
 * Scale to fit: fit bar like handles to door size, min width defined by width
 * Space: space left on borders with scale to fit active
 
Countertop
 * Enable: generate countertop
 * Depth
 * Extent
 * Chanfer: bevel size

Baseboard
 * Height: base height
 * Sink: from front
 
Cabinets: number of cabinets 
 
 
Cabinet
"""""""

.. figure:: ../img/ui_kitchen_cabinet.png
   :scale: 100 %
   :alt: Cabinet properties

   Cabinet properties

Types
 * Floor
 * Wall
 * Full

Cabinet size
 * Width
 * Depth: difference from default
 * Height: difference from default
 
Move cabinet
 * x, y, z: location from current cabinet (unless reset is checked)
 * Lock: when locked translations apply to next ones 
 * Lock: when unlocked, translation only apply to current one
 * Reset: reset location / rotation to start
 * Rotation
 
.. tip::
	Reset location in order to "stack" a group of wall cabinets over a ground of ground cabinets 
 
Countertop
 * Style
 
   * Regular
   * Sink 
   * Cookertop
   * With hole only
   * None
  
 * Fill: fill space between cabinet

Baseboard
 * Enable
 * Offset from cabinet front / sides
 * Left side: generate baseboard on left side of cabinet
 * Right side: generate baseboard on right side of cabinet
 
Doors
 * Number of doors (vertical)
 * Door style
 
   * Drawer
   * Left, right, top, double doors with and without glass
   * Oven
   * Rangehood
   * Dishwasher
   * Simple board
   * None
   
 * Height: either absolute or multiplier (setting multiplier to 0 allow absolute sizig too)
 * Number of shelves: when apply
 
   
   
.. _reference_point:

Reference point
---------------

Main properties
"""""""""""""""

.. figure:: ../img/ui_ref_2d.png
   :scale: 100 %
   :alt: Reference point (3d space)

   Reference point (3d space)

.. figure:: ../img/ui_ref_3d.png
   :scale: 100 %
   :alt: Reference point (2d space)

   Reference point (2d space)


Tools
"""""
 
 * Duplicate level: duplicate children objects
 * Delete level
 * Merge: select two or more references points to merge into one (the active one - last selected)
 
 
.. _custom objects:
   
Custom objects
--------------

Use any (clean and closed) mesh as custom hole or wall.

Custom wall
"""""""""""

Select the wall, press "Custom wall" in Add Archipack -> Custom objects. Undo by selecting the wall and press "selected" in Archipack Tools -> Kill parameters.
Custom walls must be two sided.

Custom hole
"""""""""""

Select the hole, press "Custom hole" in Add Archipack -> Custom objects. Undo by selecting the hole and press "X" next to "Custom hole".


Custom opening
""""""""""""""

Opening should be made of main and parts as childrens of main (including a custom hole object)
Select base of your opening press the "Suzan" button to create a custom opening.
Use the lines to define wich vertices are part of left/right inside/outside, top/bottom parts of the objects
Then, with base selected you'll be able to "draw" your openings onto walls (including custom ones)

